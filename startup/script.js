"use strict";
const header = document.querySelector(`header`)
const nav = document.querySelector(`.start-menu-wrap`)

const headerSticky = function (entries) {
    const [entry] = entries
    if (entry.isIntersecting === false) nav.classList.add(`sticky-nav`)
    else nav.classList.remove(`sticky-nav`)
}


const headerObserver = new IntersectionObserver(headerSticky, {
    root: null,
    threshold: 0,
})
 
 headerObserver.observe(header)