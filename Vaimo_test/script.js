"use strict";

const navEl = document.querySelector(`nav`);
const sectionEl = document.querySelector(`.register`);

const obsFunc = function (entries) {
    const [entry] = entries
    if (entry.isIntersecting === true) navEl.classList.add(`sticky`)
    else navEl.classList.remove(`sticky`)
 }

const obsOpt = {
    root: null,
    threshold: .2,
}

const observer = new IntersectionObserver(obsFunc, obsOpt)
observer.observe(sectionEl)


